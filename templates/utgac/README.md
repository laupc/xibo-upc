# Plantilla Camins

![Plantilla Camins](v1-screenshot.jpg "Plantilla UTGAC")

Plantilla utilitzada a les pantalles del la UTG de l'Àmbit de Camins.

Aquesta plantilla integra el logo de l'Escola de Camins però estan disponibles els [fitxers font](v1-background.zip "Fons Background") per si cal modificar-lo.