# Xibo UPC

Informació sobre el sistema de pantalles informatives de la UPC basat en Xibo (CMS versió 3).

## Servidor

[Històric d'actualitzacions](CHANGELOG.md)

### Docker 

 - [Configuració del servei mitjançant docker-compose](docs/server-docker-compose.md)
 - [Actualització del servei](docs/server-docker-compose-update.md)

### Kubernetes 

 - [Configuració del servei](docs/server-k8s.md)

## Clients

| Sistema Operatiu  | Hardware | Cost (any) | Vídeo HD | Vídeo 4K | Comentaris |
| :--------------: | :------: | -------------: | :------: | :------: | ---------- |
| Android (Lineage) | RPI4 B+ 8Gb | 120 € (2020) | Sí | No | Per parar i posar en marxa tant les RPIs com les pantalles s'utilitzen endolls wifi [TP-Link Tapo P100](https://www.tp-link.com/es/home-networking/smart-plug/tapo-p100/) (inclòs en el cost). |
| Android 10.0 | Bqeel TV Box 10 RK3318 | 100€ (2022) | Sí | ? | Per parar i posar en marxa tant les RPIs com les pantalles s'utilitzen endolls wifi [TP-Link Tapo P100](https://www.tp-link.com/es/home-networking/smart-plug/tapo-p100/) (inclòs en el cost). |

- [Android sobre Raspberry Pi 4](docs/client-android-rpi4.md)
- **[TODO]** [Windows](docs/client-windows.md)

## Plantilles

| Previsualització | Descripció de la plantilla | Fitxer |
--- | --- | ---
| ![Plantilla Camins](templates/utgac/v1-thumbnail.jpg "Plantilla UTGAC") | Plantilla utilitzada a les pantalles de la UTG d'Àmbit Camins (v1.0.0). [Més informació](templates/utgac/README.md) | [Descarrega](templates/utgac/v1.0.0.zip) |
| ![Plantilla CBL Info](templates/cbl/info-thumbnail.jpg "Plantilla CBL Info") | | [Descarrega](templates/cbl/info.zip) |
| ![Plantilla CBL Activitats](templates/cbl/activitats-thumbnail.jpg "Plantilla CBL Activitats") | | [Descarrega](templates/cbl/activitats.zip) |
| ![Plantilla UTGM](templates/utgm/v1-thumbnail.jpg "Plantilla UTGM") | | [Descarrega](templates/utgm/v1.zip) |
