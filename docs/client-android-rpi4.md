# Client de Xibo per Android RPI

## Instal·lació del sistema

Aquest procediment permet instal·lar Android sobre una Raspberry RPI4 B+ (4Gb). Com a distribució utilitzarem [LineageOS](https://lineageos.org/). Cal tenir en compte que actualment aquesta distribució no està suportada oficialment per l'arquitectura de la Raspberry, per tant, utilitzarem una **imatge no oficial**.

El procediment d'instal·lació és el següent:

1. Descarregar la imatge [lineage-17.1-20210122-UNOFFICIAL-KonstaKANG-rpi4.zip](https://androidfilehost.com/?fid=17248734326145720259).
1. Descomprimir i clonar la imatge sobre la targeta SD. Per fer-ho es pot utilitzar el software [balenaEtcher](https://www.balena.io/etcher).
1. Posar la targeta SD a la Raspberry i seguir les intruccions d'instal·lació. En aquest punt és important definir la zona horària correctament (Europe/Madrid GTM +1).

## Instal·lació del client de Xibo

1. Baixar el [fitxer _.APK_](https://xibo.org.uk/xibo-for-android) i mitjançant un pendrive passar-lo a la Raspberry.
1. Iniciar el client de Xibo i configurar l'adreça del servidor i el codi d'accéss en cas que sigui necessari. 
2. A *> System > Aplicacions i notificacions > Aplicacions predeterminades > Aplicació d'inici > Xibo*


## Configurar les adreces del servidor XMR

Aquesta configuració només es necessària en cas de no muntar el servei sobre un servidor on no tingui accés als ports corresponents. Per exemple, en el cas d'utilitzar k8s.

### Configurar l'adreça pública de XMR

1. Des de Xibo accedir a "Preferences > Screens".
1. Canviar l'adreça pública XMR per la que correspongui (per exemple, tcp://xibo.caminstech.upc.edu:9505).

### Configurar l'adreça privada XMR

1. Repetir els pasos 1 i 2 de l'apartat anterior
1. Canviem la direcció privada XMR per la que correspongui (per exemple, tcp://xibo-xmr-private:50001).

## Clonar imatge SD

**Atenció**. Per tal que funcioni el sistema de llicències de Xibo no és viable clonar les imatges SD.
