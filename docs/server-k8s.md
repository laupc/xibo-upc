# Servidor de Xibo sobre Kubernetes

Aquest document mostra alguns dels objectes necessaris per configurar el servidor Xibo sobre Kubernetes. 

**Atenció:** Al codi *yaml* poden aparèixer valors entre *%%* que caldrà substituir pels valors que correspongui.

## Deployments

### Xibo CMS

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: xibo
  namespace: xibo
  labels:
    app: xibo
spec:
  selector:
    matchLabels:
      app: xibo
  template:
    metadata:
      labels:
        app: xibo
    spec:
      containers:
        - name: xibo
          image: xibosignage/xibo-cms:release-2.3.10
          resources:
            requests:
              memory: "512Mi"
            limits:
              memory: "1Gi"
          ports:
            - containerPort: 80
          env:
            - name: MYSQL_HOSTNAME
              value: "%%mysql-server%%"
            - name: MYSQL_DATABASE
              valueFrom:
                secretKeyRef:
                  name: database
                  key: DATABASE
            - name: MYSQL_USER
              valueFrom:
                secretKeyRef:
                  name: database
                  key: USERNAME
            - name: MYSQL_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: database
                  key: PASSWORD
            - name: XMR_HOST
              value: "xibo-xmr"
            - name: CMS_SERVER_NAME
              value: "%%xibo-server-name%%"
          volumeMounts:
            - name: data
              subPath: cms/custom
              mountPath: /var/www/cms/custom
            - name: data
              subPath: cms/library
              mountPath: /var/www/cms/library
            - name: data
              subPath: cms/userscripts
              mountPath: /var/www/cms/web/userscripts
            - name: data
              subPath: cms/theme
              mountPath: /var/www/cms/web/theme/custom
            - name: data
              subPath: backup
              mountPath: /var/www/backup
      volumes:
        - name: data
          persistentVolumeClaim:
            claimName: %%data-claim-name%%
```

### Xibo XMR

```yml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: xibo-xmr
  namespace: xibo
  labels:
    app: xibo-xmr
spec:
  selector:
    matchLabels:
      app: xibo-xmr
  template:
    metadata:
      labels:
        app: xibo-xmr
    spec:
      containers:
        - name: xibo-xmr
          image: xibosignage/xibo-xmr:release-0.8
          resources:
            requests:
              memory: "256Mi"
            limits:
              memory: "512Mi"
          ports:
            - containerPort: 9505
              name: xmr-public
            - containerPort: 50001
              name: xmr-private
```
## Services

### Xibo CMS

```yaml
apiVersion: v1
kind: Service
metadata:
  name: xibo
  namespace: xibo
  labels:
    app: xibo
spec:
  ports:
    - port: 80
      targetPort: 80
  selector:
    app: xibo
```

### Xibo XMR

Cal tenir en compte que aquest no és un servei HTTP i, per tant, caldrà configurar un servei NodePort per poder accedir-ho des de fora de Kubernetes. 

```yaml
apiVersion: v1
kind: Service
metadata:
  name: xibo-xmr-private
  namespace: xibo
  labels:
    app: xibo-xmr-private
spec:
  ports:
    - port: 50001
      targetPort: 50001
  selector:
    app: xibo-xmr
---
apiVersion: v1
kind: Service
metadata:
  name: xibo-xmr
  namespace: xibo
  labels:
    app: xibo-xmr
spec:
  type: NodePort
  ports:
    - port: 9505
      targetPort: 9505
      nodePort: %%private-port-xmr%%
      protocol: TCP
  selector:
    app: xibo-xmr
```

## Secrets ##

```yaml
apiVersion: v1
kind: Secret
metadata:
  namespace: xibo
  name: database
type: Opaque
data:
  DATABASE: %%database-name%%
  PASSWORD: %%database-password%%
  USERNAME: %%database-username%%
```

## Ingress 

```yaml
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: xibo
  namespace: xibo
spec:
  rules:
  - host: "%%xibo-server-name%%"
    http:
      paths:
      - backend:
          serviceName: xibo
          servicePort: 80
```
