# Instal·lació de Xibo CMS 3.1.1 

Definim les variables necessàries per a la instal·lació.

```sh
export XIBO_CMS=3.1.1
export XIBO_MYSQL_PASSWORD=XXXXXXXX
```

Descarreguem i descomprimim la nova versió i la posem a `/opt`.

```sh
sudo mkdir -p /opt/xibo
cd /opt/xibo/
sudo wget https://github.com/xibosignage/xibo-cms/releases/download/${XIBO_CMS}/xibo-docker.tar.gz
sudo tar -xvzf xibo-docker.tar.gz
cd /opt/xibo/xibo-docker-${XIBO_CMS}
echo MYSQL_PASSWORD=${XIBO_MYSQL_PASSWORD} | sudo tee config.env
docker-compose up -d
```
Finalment podem accedir a la pàgina web del CMS http://127.0.0.1 (xibo_admin/password).
