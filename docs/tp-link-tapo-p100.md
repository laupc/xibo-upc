# Endolls Wifi TP-Link Tapo P100

A cada pantalla s'ha instal·lat una dispositiu TP-Link Tapo 100. Aquests dispositius són endolls que es poden control·lar a través d'una xarxa Wifi. Mitjaçant un calendari, s'apaga la pantalla.

## Més informació

* [Pàgina web del fabricant](https://www.tp-link.com/es/home-networking/smart-plug/tapo-p100/)
