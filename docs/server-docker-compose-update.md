# Actualització entre versions 3.0.x. 

Descarreguem i descomprimim la nova versió i la posem a `/opt`.

Parem la versió antiga:

```
cd /opt/xibo-docker-3.0.a 
docker-compose down
```

Copiem la carpeta shared de la versió anterior i la posem dins la nova versió. També copiem el fitxer de configuració del docker:

```
cd /opt/xibo-docker-3.0.b
cp -R ../xibo-docker-3.0.a/shared/ . 
cp ../xibo-docker-3.0.a/config.env .
```

Engeguem la nova versió:

```
docker-compose up -d
```
