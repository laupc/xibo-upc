<?php
$url = 'https://www.upc.edu/ca/sala-de-premsa/actualitat-upc/RSS';
$xml = simplexml_load_string(file_get_contents($url));
$json = [];
foreach($xml->item as $position => $element) {
	if ((string)($element->description)) {
  		$json[] = [
			'titol' => (string)($element->title),
			'resum' => (string)($element->description),
			'imatge' => [ 'src' => (string)($element->link).'/@@images/imatge/mini', 'alt' => '' ],
  		];
	}
}
header("Last-Modified: ".date('l jS \of F Y h:i:s A'));
header('Content-Type: application/json; charset=utf-8');
echo(json_encode($json));
?>
