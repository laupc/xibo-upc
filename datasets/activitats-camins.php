<?php
  $url = 'https://actualitat.camins.upc.edu/ca/activitats.json/4';
  $json = file_get_contents($url);
  $elements = json_decode($json, $associative = true);
  header("Last-Modified: ".date('l jS \of F Y h:i:s A'));
  header('Content-Type: application/json; charset=utf-8');
  echo($json);
?>
