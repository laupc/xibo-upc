## Actualitzacions 

## 2022-03-30

Actualització xibosignage/xibo-cms:release-3.0.6 a xibosignage/xibo-cms:release-3.1.0
Actualització xibosignage/xibo-xmr:0.8 a xibosignage/xibo-xmr:0.9
 
### Procediment

- Aturar el servei
- Realitzar backup de la base de dades
- Realitzar backup del sistema de fitxers 
    - /var/www/cms/custom
    - /var/www/cms/library
    - /var/www/cms/web/userscripts
    - /var/www/cms/web/theme/custom
    - /var/www/backup
- Iniciar el servei amb la nova versió. Cal tenir en compte que executa diverses migracions i, per tant, pot trigar uns minuts en estar disponible.

### Problemes detectats

- No mostra les miniatures de les composicions.
- [FIXED] No es mostra correctament el widget de la previsió meteorològica, mostra la imatge de fons per defecte. Es possible tornar-la a desactivar des del propi widget (Plantilles -> Previs -> Imatge de fons -> Cap). 


